#include <iostream>

using namespace std;

/**
 * Ermittelt, ob eine Zahl prim ist
 * Paramater:
 * x: Die zu untersuchende Zahl
 * Rückgabe: wahr, falls x eine Primzahl ist, sonst falsch
 */
bool istPrim(int x)
{
    for(int i = (x - 1); i > 1; i -= 1)
    {
        // ist x durch i teilbar?
        if((x % i) == 0)
        {
            return false;
        }
    }
    // Schleife ist ganz durchgelaufen => x ist Primzahl
    return true; 
}

/**
 * Gibt die ersten n Primzahlen auf dem Bildschirm aus
 * Parameter:
 * n: Anzahl der Primzahlen
 * Rückgabe: keine
 */
void primzahlen(int n)
{
    // 1 ist keine Primzahl, also 2 die erste
    int zahl = 2;
    for(int i = 0; i < n; i += 1)
    {
        while(!istPrim(zahl))
        {
            // Nächste Primzahl suchen
            zahl += 1;
        }
        // Bildschirmausgabe
        cout << zahl << " ";
        // Nächste Zahl 
        zahl += 1;
    }
}

int main()
{
    primzahlen(10);
    return 0;
}