// http://www.hsg-kl.de/faecher/inf/algorithmus/standard/heron/heron2.gif

#include <iostream>

using namespace std;

/**
 * Zieht die Wurzel nach dem Heron-Verfahren
 *  (übersetzt aus Struktogramm)
 * Parameter:
 * a: Die Zahl, aus der die Wurzel gezogen wird
 * epsilon: Die Genauigkeit
 * Rückgabe: Das Ergebnis (x); es gilt x² = a (näherungsweise)
 */
double heron(double a, double epsilon)
{
    double x = 0.0;
    double y = 0.0;
    if(a > 1)
    {
       x = a;
       y = 1;
    }
    else
    {
        x = 1;
        y = a;
    }
    while((x - y) > epsilon)
    {
        x = (x + y) / 2;
        y = a / x;
    }
    
    return x;
}

int main()
{
    cout << heron(23, 0.001) << endl;
    return 0;
}