#include <iostream>

using namespace std;

/**
 * Dient zu Demonstrationszwecken:
 *  Quadriert die übergebene Zahl
 * Parameter:
 *  x: Die Zahl
 * Rückgabe: keine
 */
void funktion(int& x)
{
    x = x * x;
    cout << "In der Funktion: " << x << endl;
}

/**
 * Dient zu Demonstrationszwecken:
 *  Addiert zu allen Elementen eines Feldes 1 dazu
 * Parameter:
 *  y: das Feld
 *  n: dessen Größe
 * Rückgabe: keine
 */
void funktion2(int y[], int n)
{
    // y[0] = y[0] + 1;
    for(int i = 0; i < n; i += 1)
    {
        y[i] = y[i] + 1;
    }
}

int main()
{
    int zahl = 4;
    cout << "Vorher: " << zahl << endl;
    funktion(zahl);
    cout << "Nachher: " << zahl << endl;

    int feld[5] = {2};
    cout << "Vorher: " << feld[0] << endl;
    funktion2(feld);
    cout << "Nachher: " << feld[0] << endl;

    return 0;
}
