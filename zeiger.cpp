#include <iostream>

using namespace std;

int main()
{
    cout << "Größe eines int in Byte: " << sizeof(int) << endl;

    int x = 5;
    int* z = &x;

    cout << "Adresse von x: " << z << endl;

    cout << "Wert von x: " << x << endl;

    cout << "Wert von x (über den Zeiger): " << *z << endl;

    // Ein Feld ist ein Zeiger?
    int feld[10] = {0};

    cout << feld[4] << endl;

    feld[0] = 42;
    cout << *feld << endl;
    // Ja

    feld[1] = 1;
    // Zugriff auf Element mit Index 1
    cout << feld[1] << endl;

    // über Zeiger
    cout << *(feld + 1) << endl;

    // Wozu Zeiger?
    // Dynamisches Feld:
    int* a;
    int n = 0;
    cout << "Feldgröße? ";
    cin >> n;
    a = new int[n];


    return 0;
}
